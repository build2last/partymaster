# coding:utf-8
from __future__ import unicode_literals
import time
from datetime import timedelta
from django.db import models
from django.contrib.auth.models import User


"""
用户行为：
用户：
1. 发布活动
2. 浏览活动
3. 注册参与活动
4. 注册账户
5. 修改账户信息

管理员：
管理用户和活动

活动信息：
开放程度：怎样看到活动信息
   发布者可以选择活动的开放程度，level 3 为所有人可见， level 2 为可以通过链接访问，level 3 的活动信息需要正确回答过验证信息才能访问。
参与人：一组数据记录参与者的信息
   【e-mail、名称元组列表】用来记录参与者信息，因为参与活动并不需要特别注册账户。
"""

default_live_time = timedelta(days=7)
OPEN_CHOICES = (
        (1, '输入验证码可见'),
        (2, '仅凭链接访问，不会在首页公开'),
        (3, '在网站首页公开'),
)

ACT_CHOICES = (
        ('academic', '学术'),
        ('social', '社交'),
        ('recruit', '招聘'),
)


class activity(models.Model):
    id = models.CharField(primary_key=True, editable=False, max_length=20)
    title = models.CharField(max_length=200, null=False, verbose_name='活动标题')
    detail = models.TextField(null=True, verbose_name='详细描述')
    host = models.CharField(max_length=400, null=False, verbose_name='举办方')
    poster = models.ForeignKey(User)
    attendees = models.TextField(null=True)
    open_level = models.IntegerField(null=False, default=3, choices=OPEN_CHOICES, verbose_name='信息开放等级')
    post_time = models.DateTimeField(auto_now_add=True)
    start_time = models.DateTimeField(null=False, verbose_name='开始时间')
    freeze_time = models.DateTimeField(null=False, verbose_name='注册冻结时间')
    tags = models.CharField(max_length=100, choices=ACT_CHOICES, verbose_name='标签')
    pass_word = models.CharField(max_length=20, verbose_name='活动密令', null=True)
    img = models.ImageField(upload_to='img', verbose_name='微信群二维码', null=True)
    def get_url(self):
        return "/activity/detail/{id}".format(id = self.id)
