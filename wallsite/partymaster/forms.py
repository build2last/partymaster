from django import forms
from django.contrib.auth.models import User
from .models import activity

from datetimewidget.widgets import DateTimeWidget


class LoginForm(forms.Form):
    username = forms.CharField(label='username', max_length=15)
    password = forms.CharField(widget=forms.PasswordInput)


class SignupForm(forms.ModelForm):
    password = forms.CharField(widget=forms.PasswordInput)

    class Meta:
        model = User
        fields = ['first_name', 'last_name', 'username', 'email', 'password']

class ActivityForm(forms.ModelForm):
    start_time = forms.DateTimeField(label='活动开始时间', widget=DateTimeWidget(usel10n=True, bootstrap_version=3))
    freeze_time = forms.DateTimeField(label='注册截止时间', widget=DateTimeWidget(usel10n=True, bootstrap_version=3))
    pass_word = forms.CharField(label='活动密令', max_length=20, initial="*")
    class Meta:
        model = activity
        fields = ["title","detail","host", "open_level", 
        "start_time", "freeze_time", "tags", "pass_word", "img"]


class SubscribeUser(forms.Form):
    name = forms.CharField(label='姓名', max_length=20, error_messages={'required': 'Please enter your name'})
    email = forms.EmailField(help_text='A valid email address, please.')

class VerifyForm(forms.Form):
    pass_word = forms.CharField(label='详情开启口令', max_length=100, error_messages={'required': '输入密令才能查看活动详情'})