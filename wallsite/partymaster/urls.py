from django.conf.urls import url
from django.conf.urls.static import static
from django.conf import settings

from .views import (index,
                    Logme,
                    Login,
                    Signup,
                    log_me_out,
                    Activity,
                    activate,
                    ActivityDetail,
                    )

app_name = "partymaster"

urlpatterns = [
    url(r'^$', index, name="index"),
    url(r'^logme$', Logme.as_view(), name="logme"),
    url(r'^activity$', Activity.as_view(), name="Activity"),
    url(r'^activity/detail/(?P<id>[0-9]+)[/]*$', ActivityDetail.as_view(), name="show_detail"),
    url(r'^activity/detail/', ActivityDetail.as_view(), name="show_detail"),
    url(r'^activity/(?P<user>\w+)', Activity.as_view(), name='show_my_activity'),
    url(r'^activity/(?P<user>\w+)', Activity.as_view(), name='activity_created_by_me'),
    url(r'^activity', Activity.as_view(), name="activityCRUD"),
    url(r'^login$', Login.as_view(), name="login"),
    url(r'^signup$', Signup.as_view(), name="signup"),
    url(r'^activate/(?P<accode>\w+)$', activate, name='activate'),
    url(r'^logout$', log_me_out, name="logout"),
]

urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
