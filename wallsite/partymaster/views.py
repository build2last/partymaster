# coding:utf-8
from datetime import datetime, timedelta
import time
import logging
import os
from PIL import Image

from django.shortcuts import render, redirect
from django.shortcuts import get_object_or_404
from django.http import HttpResponse
from django.views.generic import View
from django.contrib.auth.models import User
from django.contrib.auth import authenticate, login, logout
from django.views.decorators.http import require_http_methods
from django.contrib.auth.decorators import login_required
from django.core.mail import send_mail
from django.conf import settings
from .forms import(LoginForm,
                   SignupForm,
                   ActivityForm,
                   SubscribeUser,
                   VerifyForm,
                   )
from .models import activity

# from .maintainer import main
# main.every_1_day_task()

# Get an instance of a logger
logger = logging.getLogger(__name__)


# 生成 activity 的算法
def get_Aid():
    a, b = str(time.time()).split('.')
    return str(int(a)*int(b))


def get_activate_id(id, username: str):
    return str(id) + str(hex(int("".join(map(lambda x:str(ord(x)), username)))))


def index(request):
    return Activity.as_view()(request)


class Activity(View):
    def get(self, request, *args, **kwargs):
        user_name = kwargs.get('user', '')
        period_str = kwargs.get('period', '')
        tag = request.GET.get('tag', '')
        period = timedelta(
            days=(min(3, int(period_str)) if period_str.isdigit() else 3))	# 限制显示的活动时间范围
        # route /activity/
        if not user_name:
            activities = activity.objects.filter(start_time__gte=datetime.now()-period).filter(open_level__gte=3).order_by(
                '-post_time')  # .exclude(title__isnull=True).exclude(title__exact='').order_by('-comments_number')[:20]
            if tag:
                activities = activities.filter(tags=tag)
            for a_i in activities:
                a_i.people_count = a_i.attendees.count(";")
            return render(request, 'activity/activities.html',
                          {
                              "activities": activities,
                          }
                )
        # route /activity/my
        elif request.user.is_authenticated() and user_name=="my":
            type = request.GET.get('t', '1')
            # Joined activities about current user
            if type == '1':
                activities = activity.objects.filter(
                    attendees__contains=request.user.email)
            # Activities created by current user
            elif type == '2':
                start_str = request.user.first_name + \
                            request.user.last_name + "," \
                            + request.user.email + ";"
                activities = activity.objects.filter(attendees__contains=start_str)
            if type in ["1", "2"]:
                if tag:
                    activities = activities.filter(tags=tag)
                for a_i in activities:
                    a_i.people_count = a_i.attendees.count(";")
                return render(request, 'activity/activities.html',
                              {
                                  "activities": activities,
                              }
                              )
            elif type == "3":
                form = ActivityForm()
                context = {
                    'form': form,
                    'feedback': "",
                }
                # Page to create activity
                return render(request, 'activity/createActivity.html', context)
        else:
            return redirect('partymaster:Activity')

    def post(self, request, *args, **kwargs):
        # 获取来自Form的表单数据
        form = ActivityForm(request.POST or None, request.FILES or None)
        if form.is_valid():
            # 创建实例，需要做些数据处理，暂不做保存
            instance = form.save(commit=False)
            instance.id = get_Aid()
            # 将登录用户作为登记人
            instance.poster = request.user
            instance.attendees = request.user.first_name + request.user.last_name + "," \
                                 + request.user.email + ";"
            # 保存该实例
            instance.save()
            # 对非公开链接发送邮件给活动发布人
            if instance.open_level < 3:
                try:
                    user = instance.poster
                    activity_link = "http://" + settings.HOST_ADDRESS + "/activity/detail/" + str(instance.id)
                    send_mail('您发起了活动 {TITLE}'.format(TITLE=instance.title),
                              "{FirstName} {LastName},\n您的活动链接是 {LINK}".format(
                                  FirstName=user.first_name,
                                  LastName=user.last_name,
                                  LINK=activity_link),
                              settings.EMAIL_HOST_USER,
                              [user.email],
                              fail_silently=False, )
                except Exception as e:
                    return HttpResponse('Problem sending activity information link:' +
                                        str(e))
            # 跳转至列表页面
            return redirect('partymaster:Activity')
            # 创建context来集中处理需要传递到页面的数据
        context = {
            'form': form,
            'feedback': "",
        }
        # 如果没有有效提交，则仍留在原来页面
        return render(request, 'activity/createActivity.html', context)


class ActivityDetail(View):
    form_class = SubscribeUser

    def post(self, request, *args, **kwargs):
        """ 安全漏洞：如果用户直接向此链接用 post 方式提交email和user数据就可以订阅活动，
            而不需要输入验证码，此处采用更复杂的活动id来减小风险
        """
        tag = request.GET.get('t', '')
        act = get_object_or_404(activity, id=kwargs["id"])
        # 更新二维码图片
        if "image" in request.FILES and request.user.is_authenticated() and \
                act.attendees.startswith(
                    request.user.first_name +
                    request.user.last_name + "," 
                    + request.user.email + ";"):
            image = request.FILES["image"]
            img= Image.open(image)
            img.thumbnail((200,200), Image.ANTIALIAS)
            img_path = (settings.BASE_DIR + act.img.url).replace("\\", "/")
            img.save(img_path,"jpeg")
            return render(request, 'activity/sub_success.html',
                          {'msg': '<h3>头像修改成功</h3>'})
        if "image" not in request.FILES and tag == "1":
            return redirect(request.path)
        if act.open_level <= 1:
            form = VerifyForm(request.POST or None)
            if form.is_valid():
                pass_word = form.cleaned_data['pass_word']
                if pass_word != act.pass_word:
                    context_data = {"form": form, 'feedback': "口令错误"}
                    return render(request, 'activity/verify.html', context_data)
                context_data = {"activity": act, "form": SubscribeUser()}
                return render(request, 'activity/detail.html', context_data)
            if "email" not in request.POST:
                context_data = {"form": VerifyForm(), 'feedback': ""}
                return render(request, 'activity/verify.html', context_data)
        if request.user.is_authenticated():
            name = (request.user.first_name + request.user.last_name)
            email = request.user.email
            if email in act.attendees:
                return render(request, 'activity/sub_success.html',
                              {'msg': '<h3>你的邮箱已被加入过了！</h3>'})
            append_str = "%s,%s;"%(name,email)
            act.attendees += append_str
            act.save()
            return render(request, 'activity/sub_success.html',
                          {'msg': '<h3>订阅成功</h3>'})
        form = self.form_class(request.POST or None)
        if form.is_valid():  # 如果提交的数据合法
            name = form.cleaned_data['name']
            email = form.cleaned_data['email']
            if email in act.attendees:
                return render(request, 'activity/sub_success.html',
                              {'msg': '<h3>你的邮箱已被加入过了！</h3>'})
            append_str = "%s,%s;"%(name,email)
            act.attendees += append_str
            act.save()
            return render(request, 'activity/sub_success.html',
                          {'msg': '<h3>订阅成功</h3>'})
        context_data = {"activity": act, "form": form}
        return render(request, 'activity/detail.html', context_data)

    def get(self, request, *args, **kwargs):
        act = get_object_or_404(activity, id=kwargs["id"])
        if act.open_level <= 1:
            context_data = {"form": VerifyForm(), 'feedback': ""}
            return render(request, 'activity/verify.html', context_data)
        # 判断已经登陆的用户是不是这个活动的发起者
        if request.user.is_authenticated() and \
            act.attendees.startswith(
                request.user.first_name +
                request.user.last_name + "," 
                + request.user.email + ";"):
            context_data = {"activity":act, "IsOwner":True}
        else:
            Form = SubscribeUser()
            context_data = {"activity":act, "form":Form}
        context_data["random_str"] = str(time.time())
        if datetime.now() > act.start_time:
            context_data["out_of_date"] = True
        return render(request, 'activity/detail.html', context_data)


def check_login_status(request):
    if not request.user.is_authenticated():
        return redirect('partymaster:index')


class Logme(View):
    def get(self, request, *args, **kwargs):
        loginForm = LoginForm()
        signupForm = SignupForm()
        context_data = {
            'loginForm': loginForm,
            'signupForm': signupForm
        }
        return render(request, 'login.html', context_data)


class Login(View):
    form_class = LoginForm

    def post(self, request, *args, **kwargs):
        loginForm = self.form_class(request.POST or None)
        signupForm = SignupForm()
        context_data = {
            'loginForm': loginForm,
            'signupForm': signupForm
        }
        if loginForm.is_valid():
            username = loginForm.cleaned_data['username']
            password = loginForm.cleaned_data['password']
            user = get_object_or_404(User, username=username)
            if not user.is_active:
                context_data['msg_err'] = 'Please verify your email before logging in.'
                return render(request, 'login.html', context_data)
            user = authenticate(username=username, password=password)
            if user is None:
                context_data['msg_err'] = 'Invalid Credentials.'
                return render(request, 'login.html', context_data)
            else:
                login(request, user)
                return redirect('partymaster:Activity')
        context_data['msg_err'] = 'Details Not Valid'
        return render(request, 'login.html', context_data)


class Signup(View):
    form_class = SignupForm

    def post(self, request, *args, **kwargs):
        signupForm = self.form_class(request.POST or None)
        loginForm = LoginForm()
        context_data = {
            'loginForm': loginForm,
            'signupForm': signupForm
        }
        if signupForm.is_valid():
            user = signupForm.save(commit=False)
            password = signupForm.cleaned_data['password']
            user.set_password(password)
            user.is_active = False
            user.save()
            try:
                send_mail('Verify Your Email', "{FirstName} {LastName},\nWelcome to PartyWall!Please click \nhttp://{HOST}/activate/{ID}\nto verify your email".format(
                        FirstName=user.first_name,
                        LastName=user.last_name,
                        HOST=settings.HOST_ADDRESS, ID=get_activate_id(user.id,
                                                                       user.username)),
                    settings.EMAIL_HOST_USER,
                    [user.email],
                    fail_silently=False,)
            except Exception as e:
                return HttpResponse('Problem sending verification email' + str(e))
            context_data['msg_suc'] = '验证邮件已发送-> %s'%user.email
            return render(request, 'login.html', context_data)
        context_data['msg_err'] = 'Details Not Valid'
        return render(request, 'login.html', context_data)


class Profile(View):
    def get(self, request, *args, **kwargs):
        if not request.user.is_authenticated():
            return redirect(settings.LOGIN_URL)
        return render(request, 'profile.html', {'user': request.user})


def activate(request, accode):
    msg = "Nothing happened"
    print(accode.split("0x",1))
    try:
        id = accode.split("0x",1)[0]
        user = User.objects.get(id=id)
        if accode == get_activate_id(id, user.username):
            user.is_active = True
            user.save()
            msg = '<h3>Email has been verified<br/>Click <a href="/">here</a> to login</h3>'
        else:
            msg = 'You are kidding!'
    except Exception as e:
        logger.exception(e)
        msg = '<h3>Bad request!</h3>'
    finally:
        return render(request, 'activate.html', {'msg': msg})


def log_me_out(request):
    logout(request)
    return redirect('partymaster:index')
