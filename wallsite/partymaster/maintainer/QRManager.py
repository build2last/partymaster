#coding:utf-8
"""删除文件夹中修改日期在7天以前的指定类型文件"""
__python__ = 3

import os
import time
import logging

logging.basicConfig(filename='maintainer.log', level=logging.DEBUG, format='%(asctime)s  %(filename)s[line:%(lineno)d] %(levelname)s %(message)s', datefmt='%a, %d %b %Y %H:%M:%S')

A_WEEK_IN_SECOND = 3600 * 24 * 7

file_extension = (".jpg", ".png", ".gif")

def delete_outdate_qrcode_in_dir(dir_path):
    qrcode_list = filter(lambda x:x.lower().endswith(file_extension), os.listdir(dir_path))
    for qrcode in qrcode_list:
        file_path = os.path.join(dir_path, qrcode)
        if time.time() > (os.path.getmtime(file_path) + A_WEEK_IN_SECOND):
            os.remove(file_path)
            logging.info("%s has been deleted for out-of-date"%qrcode)

def unit_test():
    delete_outdate_qrcode_in_dir(os.getcwd())

if __name__ == '__main__':
    unit_test()