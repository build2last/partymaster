from .QRManager import delete_outdate_qrcode_in_dir
from django.conf import settings
import threading

DAY_1_IN_SECOND = 3600*24
def every_1_day_task():
	delete_outdate_qrcode_in_dir(settings.MEDIA_ROOT)
	t = threading.Timer(DAY_1_IN_SECOND, every_1_day_task)
	t.start()
	